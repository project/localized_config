(function ($) {
  'use strict';

  Drupal.behaviors.localizedConfig = {
    attach: function (context, settings) {
      // Get the vertical tab element clicks.
      $('.localized-config-form .vertical-tabs__menu-item', context).click(function () {
        // Get the hidden plugin fields.
        var openPluginField = $('.open-module', '.localized-config-form');

        // Get which tab was clicked.
        var tabIndex = $(this).index();

        // Get the corresponding details element.
        var detailsElement = $('details.vertical-tabs__pane', '.vertical-tabs__panes').eq(tabIndex);

        // Save to the hidden field.
        $(openPluginField).val($(detailsElement).data('drupal-selector'));

        // Assign hashes for consistent navigation.
        var str_hash = $('a', this).attr('href').substr(1);
        history.replaceState(null, null, '#' + str_hash);
        $('.localized-config-form .locale-settings-menu .menu-item > a').each(function () {
          var href = $(this).attr('href').split('#')[0] + '#' + str_hash;
          $(this).attr('href', href);
        });
      });
    }
  };
})(jQuery);
