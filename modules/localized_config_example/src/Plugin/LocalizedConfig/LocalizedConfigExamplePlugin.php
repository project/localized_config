<?php

namespace Drupal\localized_config_example\Plugin\LocalizedConfig;

use Drupal\Core\Config\StorableConfigBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\localized_config\LocalizedConfigPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * An example Localized Config plugin.
 *
 * @Plugin(
 *   title = @Translation("Example"),
 *   id = "example",
 *   weight = 0,
 *   disabled = 0,
 * )
 */
class LocalizedConfigExamplePlugin extends LocalizedConfigPluginBase {

  /**
   * {@inheritdoc}
   */
  public function add(StorableConfigBase $config, $language, array &$form, FormStateInterface $form_state) {
    $element['example_setting'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Example setting'),
      '#required' => TRUE,
      '#description' => $this->t('This is an example setting. Enter "test" to try out error validation.'),
      '#default_value' => $config->get('example_setting'),
    ];

    $element['example_setting_file'] = [
      '#title' => $this->t('File'),
      '#type' => 'managed_file',
      '#description' => $this->t('Upload a file.'),
      '#default_value' => $config->get('example_setting_file'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(StorableConfigBase $config, array $values, array &$form, FormStateInterface $form_state, $language) {
    if ($values['example_setting'] === 'test') {
      $form_state->setError($form['module_example']['example_setting'], $this->t('Example element: "test" is not allowed.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submit(StorableConfigBase $config, array $values, array &$form, FormStateInterface $form_state, $language) {

    // Logic handling values.
    $config->set('example_setting', $values['example_setting'] ? $values['example_setting'] : NULL);
    $config->set('example_setting_file', $values['example_setting_file'] ? $values['example_setting_file'] : NULL);

    // Files always have to be setPermanent and saved. Otherwise the
    // garbage collector may remove them.
    $file = $values['example_setting_file'] ? File::load($values['example_setting_file'][0]) : NULL;
    if ($file instanceof FileInterface) {
      $file->setPermanent();
      try {
        $file->save();
      }
      catch (EntityStorageException $e) {
        $this->messenger()->addWarning($this->t('Something went wrong saving the file.'));
      }
    }

    $config->save();
  }

}
