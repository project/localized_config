<?php

namespace Drupal\localized_config;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\Entity\User;

/**
 * Helper for locale settings/variable-related methods.
 *
 * @package Drupal\localized_config
 */
class LocalizedConfigLanguageHelper {

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The configuration for the Localized Config module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * LocalizedConfigLanguageHelper constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager, AccountProxyInterface $current_user, ModuleHandlerInterface $module_handler) {
    $this->configFactory = $config_factory;
    $this->languageManager = $language_manager;
    $this->currentUser = $current_user;
    $this->moduleHandler = $module_handler;

    $this->config = $this->configFactory->get('localized_config.settings');
  }

  /**
   * Retrieves a list of all languages which have a site code attached.
   *
   * @param \Drupal\Core\Language\LanguageInterface[]|null $languages
   *   An array of languages.
   *
   * @return \Drupal\Core\Language\LanguageInterface[]
   *   An array of languages which correspond to a site.
   */
  public function getFilteredLanguages($languages = NULL) {
    // Fetch default.
    if (!$languages) {
      $languages = $this->languageManager->getNativeLanguages();
    }

    if (!empty($this->config->get('filter_sitecode'))) {
      // Filter out all languages which do not have a site code.
      foreach ($languages as $langcode => $language) {
        $parts = explode('-', $langcode);
        if (count($parts) < 2) {
          unset($languages[$langcode]);
        }
      }
    }

    return $languages;
  }

  /**
   * Returns a list of enabled languages.
   *
   * @param \Drupal\Core\Language\LanguageInterface[]|null $languages
   *   If not provided, all languages will be filtered.
   *
   * @return \Drupal\Core\Language\LanguageInterface[]|null
   *   Returns a filtered list of enabled languages.
   */
  public function getEnabledLanguages($languages = NULL) {
    if (!$languages) {
      $languages = $this->getFilteredLanguages();
    }

    // Support for the "Disable Language" module.
    if ($this->moduleHandler->moduleExists('disable_language')) {
      /** @var string $langcode */
      /** @var \Drupal\language\ConfigurableLanguageInterface $language */
      foreach ($languages as $langcode => $language) {
        if ($language->getThirdPartySetting('disable_language', 'disable', FALSE)) {
          unset($languages[$langcode]);
        }
      }
    }

    // Allow developers to hook into this list.
    $this->moduleHandler->alter('localized_config_language_list', $languages);

    return $languages;
  }

  /**
   * Fetches the languages associated with the user.
   *
   *   By default, all native Languages are returned, that are not filtered out
   *   via filter_sitecode setting. Any additional language restrictions have to
   *   be processed within hook_localized_config_user_languages_alter().
   *
   * @param \Drupal\user\UserInterface|\Drupal\Core\Session\AccountProxyInterface|null $user
   *   The user to query. If not provided, will use current user.
   * @param bool $include_disabled
   *   Decides if disabled languages should be counted.
   *
   * @return array[LanguageInterface]
   *   An array of languages.
   */
  public function getLanguagesOfUser($user = NULL, $include_disabled = TRUE) {
    if (!$user) {
      $user = $this->currentUser;
    }

    if ($user instanceof AccountProxyInterface) {
      $user = User::load($user->id());
    }

    // First language: The current user's language.
    $languages = [
      $user->language()->getId() => $user->language(),
    ];

    // Add all native languages, filtered.
    $languages += $this->getFilteredLanguages();

    // Let other modules change the language list for current user.
    $this->moduleHandler->alter('localized_config_user_languages', $languages, $user);

    // Filter the languages by whether they're enabled or not.
    $languages = $include_disabled ? $languages : $this->getEnabledLanguages($languages);

    return $languages;
  }

  /**
   * Languages to readable array.
   *
   * @param \Drupal\language\Entity\ConfigurableLanguage[] $languages
   *   An array of language objects.
   * @param bool $append_country
   *   Whether the country should be appended after the language name.
   *
   * @return array
   *   An array of readable languages; key is langcode, value is label.
   */
  public function languagesToReadableArray(array $languages, $append_country = TRUE): array {
    $list = [];

    /** @var \Drupal\language\Entity\ConfigurableLanguage $language */
    foreach ($languages as $language) {
      if ($append_country) {
        $list[$language->getId()] = $language->getName();
      }
      else {
        $list[$language->getId()] = preg_replace("/ \([^)]+\)/", '', $language->getName());
      }
    }

    return $list;
  }

  /**
   * Get the first accessible language by current user.
   *
   * @return string
   *   The language id or an empty string.
   */
  public function firstAccessibleLanguage() {
    // If permission for global config, no language parameter is needed.
    if ($this->currentUser->hasPermission('access global localized config')) {
      return '';
    }

    // Get the user's assigned languages.
    $languages = $this->getLanguagesOfUser($this->currentUser);

    // Get the current language.
    $current_language = $this->languageManager->getCurrentLanguage();

    // If user has access to the current language, link to that.
    if (array_key_exists($current_language->getId(), $languages)) {
      return $current_language->getId();
    }

    // Otherwise link to the first accessible language.
    $first_language = array_shift($languages);
    return $first_language ? $first_language->getId() : '';
  }

}
