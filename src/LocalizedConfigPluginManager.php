<?php

namespace Drupal\localized_config;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Localized Config plugin manager.
 *
 * @see plugin_api
 */
class LocalizedConfigPluginManager extends DefaultPluginManager {

  /**
   * Constructs a LocalizedConfigPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {

    // Will seek plugins in the LocalizedConfig folder of the src folder.
    $subdir = 'Plugin/LocalizedConfig';
    $interface = 'Drupal\localized_config\LocalizedConfigPluginInterface';
    $annotation = 'Drupal\Component\Annotation\Plugin';

    parent::__construct($subdir, $namespaces, $module_handler, $interface, $annotation);

    // This allows the plugin definitions to be altered by an alter hook.
    $this->alterInfo('localized_config_info');

    // This sets the caching method for our plugin definitions.
    $this->setCacheBackend($cache_backend, 'localized_config_info');
  }

}
