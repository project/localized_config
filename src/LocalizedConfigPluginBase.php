<?php

namespace Drupal\localized_config;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base plugin class for localized config.
 *
 * @package Drupal\localized_config
 */
abstract class LocalizedConfigPluginBase extends PluginBase implements LocalizedConfigPluginInterface, ContainerFactoryPluginInterface {

  /**
   * A helper class with useful methods pertaining to Localized Config.
   *
   * @var \Drupal\localized_config\LocalizedConfigHelper
   */
  protected $localizedConfigHelper;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param LocalizedConfigHelper $localized_config_helper
   *   Localized config helper.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LocalizedConfigHelper $localized_config_helper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->localizedConfigHelper = $localized_config_helper;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('localized_config.helper'));
  }

}
