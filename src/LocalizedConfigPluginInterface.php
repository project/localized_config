<?php

namespace Drupal\localized_config;

use Drupal\Core\Config\StorableConfigBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin interface for localized config.
 *
 * @package Drupal\localized_config
 */
interface LocalizedConfigPluginInterface {

  /**
   * Populates a form with elements to configure the plugin.
   *
   * @param \Drupal\Core\Config\StorableConfigBase $config
   *   Config object.
   * @param string|null $language
   *   Langcode of the locale currently being configured.
   * @param array $form
   *   The main form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @return array
   *   Form element.
   */
  public function add(StorableConfigBase $config, $language, array &$form, FormStateInterface $form_state);

  /**
   * Validate the results.
   *
   * @param \Drupal\Core\Config\StorableConfigBase $config
   *   Config object we will be saving our configuration to.
   * @param array $values
   *   An easier-to-read array of submitted values passed on by the main form.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string|null $language
   *   Langcode of the locale currently being configured.
   */
  public function validate(StorableConfigBase $config, array $values, array &$form, FormStateInterface $form_state, $language);

  /**
   * Submit the results.
   *
   * @param \Drupal\Core\Config\StorableConfigBase $config
   *   Config object we will be saving our configuration to.
   * @param array $values
   *   An easier-to-read array of submitted values passed on by the main form.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string|null $language
   *   Langcode of the locale currently being configured.
   *
   * @return null
   *   Return nothing.
   */
  public function submit(StorableConfigBase $config, array $values, array &$form, FormStateInterface $form_state, $language);

}
