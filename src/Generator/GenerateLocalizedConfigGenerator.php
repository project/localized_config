<?php

namespace Drupal\localized_config\Generator;

use Drupal\Console\Core\Generator\Generator;
use Drupal\Console\Core\Generator\GeneratorInterface;
use Drupal\Console\Core\Utils\TwigRenderer;
use Drupal\Console\Extension\Manager;

/**
 * Localized config plugin generator.
 *
 * @package Drupal\Console\Generator
 */
class GenerateLocalizedConfigGenerator extends Generator implements GeneratorInterface {

  /**
   * GenerateLocalizedConfigGenerator constructor.
   *
   * @param \Drupal\Console\Extension\Manager $extensionManager
   *   An extension manager.
   * @param \Drupal\Console\Core\Utils\TwigRenderer $renderer
   *   Twig renderer.
   */
  public function __construct(Manager $extensionManager, TwigRenderer $renderer) {
    $this->extensionManager = $extensionManager;

    $renderer->addSkeletonDir(__DIR__ . '/../../templates/');
    $this->setRenderer($renderer);
  }

  /**
   * {@inheritdoc}
   */
  public function generate(array $parameters) {
    // Render and copy the Localized Config Plugin file.
    $this->renderFile(
      'LocalizedConfig.php.twig',
      $parameters['module_path'] . '/src/Plugin/LocalizedConfig/' . $parameters['class'] . '.php',
      $parameters
    );
  }

}
