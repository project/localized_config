<?php

namespace Drupal\localized_config\Plugin\Menu;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Menu\MenuLinkBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\localized_config\LocalizedConfigHelper;
use Drupal\localized_config\LocalizedConfigLanguageHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates link to localized config depending on serveral conditions.
 *
 * @package Drupal\localized_config\Plugin\Menu
 */
class LocalizedConfigDynamicMenuLink extends MenuLinkBase implements ContainerFactoryPluginInterface {

  /**
   * The localized config helper service.
   *
   * @var \Drupal\localized_config\LocalizedConfigHelper
   */
  protected $localizedConfigHelper;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Localized Config language-related methods.
   *
   * @var \Drupal\localized_config\LocalizedConfigLanguageHelper
   */
  protected $languageHelper;

  /**
   * Constructs a new bubbling URL generator service.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\localized_config\LocalizedConfigHelper $localized_config_helper
   *   Localized config helper.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\localized_config\LocalizedConfigLanguageHelper $language_helper
   *   Localized Config language-related methods.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LocalizedConfigHelper $localized_config_helper, AccountInterface $current_user, LanguageManagerInterface $language_manager, LocalizedConfigLanguageHelper $language_helper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->localizedConfigHelper = $localized_config_helper;
    $this->currentUser = $current_user;
    $this->languageManager = $language_manager;
    $this->languageHelper = $language_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('localized_config.helper'),
      $container->get('current_user'),
      $container->get('language_manager'),
      $container->get('localized_config.language_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->pluginDefinition['title'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function updateLink(array $new_definition_values, $persist) {
    return $this->pluginDefinition;
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters() {
    $language_parameter = $this->localizedConfigHelper->languagesSupported() ? $this->languageHelper->firstAccessibleLanguage() : '';
    return [
      'language' => $language_parameter,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['user'];
  }

}
