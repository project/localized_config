<?php

namespace Drupal\localized_config\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration of localized config behavior.
 */
class LocalizedConfigSettings extends ConfigFormBase {

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new DefaultForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'localized_config.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'localized_config_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('localized_config.settings');

    $form['enable_languages'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable language support'),
      '#description' => $this->t('Toggle this checkbox to enable language tabs in Localized Configuration. Toggling this off will limit the interface to global configuration.'),
      '#default_value' => $config->get('enable_languages') ?? TRUE,
    ];

    $form['filter_sitecode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Filter by site code'),
      '#description' => $this->t('Remove languages from the interface whose language codes are not written in format "language-sitename", e.g. "de-kk".<br>This is useful if you use languages for multisite purposes.'),
      '#default_value' => $config->get('filter_sitecode'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('localized_config.settings')
      ->set('enable_languages', $form_state->getValue('enable_languages'))
      ->set('filter_sitecode', $form_state->getValue('filter_sitecode'))
      ->save();
  }

}
