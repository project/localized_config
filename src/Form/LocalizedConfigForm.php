<?php

namespace Drupal\localized_config\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Template\Attribute;
use Drupal\language\ConfigurableLanguageManagerInterface;
use Drupal\localized_config\LocalizedConfigHelper;
use Drupal\localized_config\LocalizedConfigLanguageHelper;
use Drupal\localized_config\LocalizedConfigPluginManager;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\Html;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * All localized config plugins.
 *
 * @package Drupal\localized_config\Form
 */
class LocalizedConfigForm extends ConfigFormBase {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The Localized Config plugin manager.
   *
   * @var \Drupal\localized_config\LocalizedConfigPluginManager
   */
  protected $localizedConfigPluginManager;

  /**
   * The Drupal default config manager.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;

  /**
   * The language manager.
   *
   * @var \Drupal\language\ConfigurableLanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The localized config helper service.
   *
   * @var \Drupal\localized_config\LocalizedConfigHelper
   */
  protected $localizedConfigHelper;

  /**
   * Curretn user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Localized Config language-related methods.
   *
   * @var \Drupal\localized_config\LocalizedConfigLanguageHelper
   */
  protected $languageHelper;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\ConfigManager $config_manager
   *   Config manager service.
   * @param \Drupal\localized_config\LocalizedConfigPluginManager $localized_config_plugin_manager
   *   Localized Config plugin manager service.
   * @param \Drupal\language\ConfigurableLanguageManagerInterface $language_manager
   *   Language manager service.
   * @param \Drupal\localized_config\LocalizedConfigHelper $localized_config_helper
   *   Config helper service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   LoggerFactoryInterface.
   * @param \Drupal\localized_config\LocalizedConfigLanguageHelper $language_helper
   *   Localized Config language-related methods.
   */
  public function __construct(ConfigFactory $config_factory, ConfigManager $config_manager, LocalizedConfigPluginManager $localized_config_plugin_manager, ConfigurableLanguageManagerInterface $language_manager, LocalizedConfigHelper $localized_config_helper, AccountInterface $current_user, RequestStack $request_stack, LoggerChannelFactoryInterface $logger, LocalizedConfigLanguageHelper $language_helper) {
    parent::__construct($config_factory);

    $this->configManager = $config_manager;
    $this->localizedConfigPluginManager = $localized_config_plugin_manager;
    $this->languageManager = $language_manager;
    $this->localizedConfigHelper = $localized_config_helper;
    $this->currentUser = $current_user;
    $this->request = $request_stack->getCurrentRequest();
    $this->logger = $logger->get('key');
    $this->languageHelper = $language_helper;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.manager'),
      $container->get('plugin.manager.localized_config'),
      $container->get('language_manager'),
      $container->get('localized_config.helper'),
      $container->get('current_user'),
      $container->get('request_stack'),
      $container->get('logger.factory'),
      $container->get('localized_config.language_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'localized_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $language = NULL) {

    // Don't cache the form.
    $form_state->disableCache();

    // Add the library for styling elements such as the locale tabs.
    $form['#attached']['library'][] = 'localized_config/styling';

    // Get the query parameter for opened module (if set).
    $query = $this->request->query->all();
    $open_module = '';
    if (isset($query['open_module'])) {
      $open_module = $query['open_module'];
    }

    // Store the language in the form state for future reference.
    $form_state->setStorage([
      'language' => $language,
    ]);

    if ($this->localizedConfigHelper->languagesSupported()) {

      // Draw the locale tabs.
      $form['languages'] = $this->renderLanguageTabs($language);
    }

    // Set up the container for the various settings.
    $form['modules'] = [
      '#type' => 'vertical_tabs',
    ];
    // Set default vertical tab when there is an opened module.
    if ($open_module) {
      $form['modules']['#default_tab'] = Html::cleanCssIdentifier($open_module);
    }

    // Add hidden field for the opened module.
    // Will be converted to redirect query parameter on submit.
    $form['open_module'] = [
      '#type' => 'hidden',
      '#default_value' => $open_module,
      '#attributes' => [
        'class' => [
          'open-module',
        ],
      ],
    ];

    // Fetch the registered config plugins from all modules...
    $plugin_definitions = $this->localizedConfigPluginManager->getDefinitions();

    // ...sort them by weight...
    $comparison = function (array $a, array $b) {
      $weight_a = $a['weight'] ?? 0;
      $weight_b = $b['weight'] ?? 0;

      // Space ship operator, available since PHP 7.0.
      return $weight_a <=> $weight_b;
    };
    uasort($plugin_definitions, $comparison);

    // ...filter out disabled plugins...
    $plugin_definitions = array_filter($plugin_definitions, function ($definition) {
      return empty($definition['disabled']);
    });

    // ...and iterate through the rest.
    $empty = TRUE;

    foreach ($plugin_definitions as $definition) {
      // Hide plugins which are considered 'global-only'.
      if (!empty($definition['global_only']) && $language !== FALSE) {
        continue;
      }

      // Only check roles IF plugin has allowed roles defined.
      if (!$this->currentUser->hasPermission('access localized config ' . $definition['id'])) {
        continue;
      }

      // Fetch the plugin's form elements.
      $plugin_form = $this->processPluginForm($definition, $language, $form, $form_state);
      if (!empty($plugin_form)) {
        $empty = FALSE;
        $form['module_' . $definition['id']] = $plugin_form;
      }
    }

    // Show message if there are no plugins.
    if ($empty) {
      $this->messenger()
        ->addWarning("No Localized Configuration plugins available.");
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $storage = $form_state->getStorage();
    $language = $storage['language'];
    $trigger = $form_state->getTriggeringElement();
    if ($trigger && $trigger["#parents"][1] === 'submit') {
      $id = str_replace('module_', '', $trigger["#parents"][0]);
      $plugin_definitions = $this->localizedConfigPluginManager->getDefinitions();
      if ($plugin_definitions[$id]) {
        $definition = $plugin_definitions[$id];
        if (!empty($values['module_' . $definition['id']])) {
          $plugin_config = $this->getSpecificConfig($definition['id'], $language);
          $plugin_values = $values['module_' . $definition['id']];

          /** @var \Drupal\localized_config\LocalizedConfigPluginInterface $plugin */
          $plugin = $this->localizedConfigPluginManager->createInstance($definition['id']);
          $plugin->validate($plugin_config, $plugin_values, $form, $form_state, $language);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $storage = $form_state->getStorage();
    $language = $storage['language'];
    $trigger = $form_state->getTriggeringElement();
    if ($trigger && $trigger["#parents"][1] === 'submit') {
      $id = str_replace('module_', '', $trigger["#parents"][0]);
      $plugin_definitions = $this->localizedConfigPluginManager->getDefinitions();
      if ($plugin_definitions[$id]) {
        $definition = $plugin_definitions[$id];
        if (!empty($values['module_' . $definition['id']])) {
          $plugin_config = $this->getSpecificConfig($definition['id'], $language);
          $plugin_values = $values['module_' . $definition['id']];

          // Clean up the values.
          unset($plugin_values['enabled']);
          unset($plugin_values['submit']);

          $plugin = $this->localizedConfigPluginManager->createInstance($definition['id']);
          $plugin->submit($plugin_config, $plugin_values, $form, $form_state, $language);

          // If in language context, check whether this module
          // has a global config. If not, create one with the
          // only value enabled: false.
          // This avoids issues with importing configuration.
          if ($language && $this->localizedConfigHelper->languagesSupported()) {
            $global_config = $this->getSpecificConfig($id, FALSE);
            if ($global_config->get('enabled') === NULL) {
              $global_config->set('enabled', FALSE)->save();
            }
          }

          // Invalidate cache.
          $this->localizedConfigHelper->invalidateConfigCache($definition['id']);
        }
      }
    }

    // Get the hidden field value for opened module.
    $open_module = $form_state->getValue('open_module', '');
    if ($open_module) {

      // Get current URL and add / replace the opened module.
      $url = Url::fromRoute('<current>');
      $query = $url->getOption('query');
      if (!is_array($query)) {
        $query = [];
      }
      $query['open_module'] = $open_module;
      $url->setOption('query', $query);

      // Set the form redirect to the new URL.
      $form_state->setRedirectUrl($url);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * Fetches the form elements from a config plugin and prepares output.
   *
   * @param array|string $definition
   *   The plugin definition.
   * @param string|null|false $language
   *   A langcode representing the current language.
   * @param array $form
   *   The main form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @return array|false
   *   The resulting form elements.
   */
  protected function processPluginForm($definition, $language, array $form, FormStateInterface $form_state) {
    // If a plugin ID is provided, fetch the full definition.
    if (is_string($definition)) {
      $definition = $this->localizedConfigPluginManager->getDefinition($definition);
    }

    $form = [];
    $duplicate_form_keys = [];

    // Fetch the config for this particular locale.
    $plugin_config = $this->getSpecificConfig($definition['id'], $language);

    // Plugin enabled state in this locale.
    $enabled = $plugin_config->get('enabled');

    // Plugin enabled state in any inherited locale.
    $inherited_enabled = $this->localizedConfigHelper->getVariable($definition['id'], 'enabled', $language);

    /*
     * Only display the form if either:
     * 1) The plugin is enabled/inherits its enabled state.
     * 2) You're an admin.
     */
    if ($inherited_enabled || (!$inherited_enabled && $this->currentUser->hasPermission('enable localized config plugins'))) {
      // The main fieldset.
      $form = [
        '#type' => 'details',
        '#group' => 'modules',
        '#title' => $definition['title'],
        '#tree' => TRUE,
      ];

      $title_addition = $this->localizedConfigHelper->languagesSupported() ? ' (' . $this->getLocaleTitle($language) . ')' : '';
      $form['title'] = [
        '#markup' => '<h2 class="plugin-title">' . $definition['title'] . $title_addition . '</h2>',
        '#weight' => -200,
      ];

      // "Enable module" button for administrators.
      $form['enabled'] = [
        '#type' => 'submit',
        '#weight' => -199,
        '#access' => $this->currentUser
          ->hasPermission('enable localized config plugins') ? TRUE : FALSE,
        '#value' => $enabled ? $this->t('Disable for this locale') : $this->t('Enable for this locale'),
        '#attributes' => [
          'data-module-id' => $definition['id'],
          'class' => [
            $enabled ? 'button--danger' : 'button--primary',
          ],
        ],
        '#name' => $definition['id'] . '_enable_submit',
        '#limit_validation_errors' => [],
        '#validate' => [
          [
            $this,
            'moduleEnableValidate',
          ],
        ],
        '#submit' => [
          [
            $this,
            'moduleEnableSubmit',
          ],
        ],
      ];

      // Build button label.
      $enable = $this->localizedConfigHelper->languagesSupported() ? 'Enable for all locales' : 'Enable';
      $disable = $this->localizedConfigHelper->languagesSupported() ? 'Disable for all locales' : 'Disable';
      if (!$language) {
        $form['enabled']['#value'] = $enabled ? $this->t('@disable', ['@disable' => $disable]) : $this->t('@enable', ['@enable' => $enable]);
      }

      // If the enabled state is inherited, you can't change it.
      if (!$enabled && $inherited_enabled) {
        $form['enabled'] = [
          '#access' => $this->currentUser
            ->hasPermission('enable localized config plugins') ? TRUE : FALSE,
          '#markup' => $this->t('Module is enabled (inherited)'),
        ];
      }

      $elements = [];
      try {
        // Create an instance of the plugin...
        $plugin = $this->localizedConfigPluginManager->createInstance($definition['id']);

        // Fetch the plugin's form elements...
        $elements = $plugin->add($plugin_config, $language, $form, $form_state);
      }
      catch (PluginException $exception) {
        $this->logger->warning('Something went wrong with plugin @plugin_id', ['@plugin_id' => $definition['id']]);
      }

      // Filter them for permissions and such...
      $this->filterElementPermissions($elements, $enabled || $inherited_enabled);

      // Check if there are form keys, that are already used by the base
      // form. Example: title.
      $duplicate_form_keys = array_intersect_key($elements, $form);

      // ...then attach them to the overlying element!
      $form += $elements;

      // Also, add a submit button.
      if ($enabled || $inherited_enabled) {
        $form['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Save @title configuration', ['@title' => $definition['title']]),
          '#description' => $this->t('For @locale locale.', ['@locale' => $this->getLocaleTitle($language)]),
          '#button_type' => 'primary',
          '#validate' => [
            [
              $this,
              'validateForm',
            ],
          ],
          '#submit' => [
            [
              $this,
              'submitForm',
            ],
          ],
        ];
      }

      foreach (array_keys($elements) as $key) {
        $form['submit']['#limit_validation_errors'][] = [
          "module_{$definition['id']}",
          $key,
        ];
      }
    }

    // Place strong warning about duplicate (overridden) form elements.
    if ($duplicate_form_keys) {
      $this->messenger()
        ->addError($this->t('<strong>Attention:</strong> There are not allowed form keys used! These will be overridden by the base form and therefore not usable/visible.<br>Please change the key names for: <strong>@keys</strong>',
          [
            '@keys' => implode(', ', array_keys($duplicate_form_keys)),
          ]
        )
        );
    }

    // Return the finalized form element, whether it has content or not.
    return $form;
  }

  /**
   * The validation for clicking a "Enable module" button.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function moduleEnableValidate(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $module_id = $triggering_element['#attributes']['data-module-id'];

    // Safety check if the user is permitted to enable/disable a module.
    if (!$this->currentUser->hasPermission('enable localized config plugins')) {
      $form_state->setError($form['module_' . $module_id]['enabled'], "You don't have the permission to do that.");
    }
  }

  /**
   * The callback for clicking a "Enable module" button.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function moduleEnableSubmit(array &$form, FormStateInterface $form_state) {
    // Fetch language from the storage.
    $storage = $form_state->getStorage();
    $language = $storage['language'];

    // Get the human-readable name for the locale.
    $locale = $this->getLocaleTitle($language);

    /*
     * The triggering element identifies which module we're enabling.
     * Don't do anything if we can't determine it.
     */
    $triggering_element = $form_state->getTriggeringElement();
    $module_id = $triggering_element['#attributes']['data-module-id'];
    if ($module_id) {
      // Fetch the plugin definition for its readable name.
      $plugin_definition = $this->localizedConfigPluginManager->getDefinition($module_id);

      $config = $this->getSpecificConfig($module_id, $language);

      // We toggle the module's enabled state.
      $enabled = $config->get('enabled');
      $enabled = !$enabled;

      // Output a nice message and wrap up the process.
      if ($enabled) {
        $this->messenger()
          ->addMessage($this->t('Successfully enabled plugin "@plugin"@locale.', [
            '@plugin' => $plugin_definition['title'],
            '@locale' => $this->localizedConfigHelper->languagesSupported() ? ' for "' . $locale . '"' : '',
          ]));

        $config->set('enabled', TRUE)->save();
      }
      else {
        $this->messenger()
          ->addMessage($this->t('Successfully disabled plugin "@plugin"@locale.', [
            '@plugin' => $plugin_definition['title'],
            '@locale' => $this->localizedConfigHelper->languagesSupported() ? ' for "' . $locale . '"' : '',
          ]));

        // If we're disabling the module, wipe the entire locale config for it.
        $config->delete();

        // Invalidate cache.
        $this->localizedConfigHelper->invalidateConfigCache($module_id);
      }
    }
  }

  /**
   * Fetch a specific module config based on language.
   *
   * @param string $id
   *   Module ID.
   * @param string $language
   *   Language to fetch.
   *
   * @return \Drupal\Core\Config\StorableConfigBase
   *   Return the config.
   */
  protected function getSpecificConfig($id, $language) {
    if ($language) {
      $config = $this->languageManager->getLanguageConfigOverride($language, 'localized_config.' . $id);
    }
    else {
      $config = $this->configFactory()->getEditable('localized_config.' . $id);
    }
    return $config;
  }

  /**
   * Display the tabs that can be used to switch between  locales.
   *
   * @param string|null $language
   *   The current language.
   *
   * @return array
   *   The rendered menu.
   */
  protected function renderLanguageTabs($language = NULL) {
    $languages = $this->languageHelper->getLanguagesOfUser();
    $items = [];

    foreach ($languages as $langcode => $lang) {
      $items[$langcode] = [
        'title' => $lang->getName(),
        'attributes' => new Attribute(),
        'url' => Url::fromRoute('localized_config.localized_config', [
          'language' => $langcode,
        ]),
        'in_active_trail' => ($lang == $langcode) ? TRUE : FALSE,
      ];
    }

    $menu = [
      '#theme' => 'menu',
      '#menu_name' => 'per_locale_settings',
      '#items' => $items,
      '#attributes' => [
        'class' => [
          'locale-settings-menu',
        ],
      ],
    ];

    return $menu;
  }

  /**
   * Get the appropriate title for the current form.
   *
   * @param string|null $language
   *   The langcode representing the current language.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Returns a fitting title.
   */
  public function getFormTitle($language = NULL) {
    $locale_title = $this->getLocaleTitle($language);
    return $this->t('Configuration for locale "@locale"', ['@locale' => $locale_title]);
  }

  /**
   * Get the appropriate title for the current locale.
   *
   * @param string|bool|null $language
   *   The code representing the locale's language.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|mixed|null|string
   *   A fitting title for the locale.
   */
  public function getLocaleTitle($language = NULL) {
    if ($language) {
      $language_object = $this->languageManager->getLanguage($language);
      if ($language_object) {
        return $language_object->getName();
      }
    }

    return $this->t('Global');
  }

  /**
   * Check form access permissions, filter out the unauthorized.
   *
   * @param array $el
   *   Form element tree.
   * @param bool $enabled
   *   Whether the module is enabled or not.
   */
  protected function filterElementPermissions(array &$el, $enabled) {

    // Check sub-elements.
    foreach ($el as &$value) {
      if (is_array($value)) {
        if (!$enabled) {

          // If element should be disabled, remove access.
          $value['#access'] = FALSE;
        }
      }
    }
  }

}
