<?php

namespace Drupal\localized_config\Config;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Language\LanguageDefault;
use Drupal\language\Config\LanguageConfigFactoryOverride;
use Drupal\language\Config\LanguageConfigFactoryOverrideInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Makes the LanguageConfigFactoryOverride friendlier to Localized Config.
 */
class LocalizedConfigFactoryOverrideDecorator extends LanguageConfigFactoryOverride {

  /**
   * The original service that is being decorated.
   *
   * @var \Drupal\language\Config\LanguageConfigFactoryOverrideInterface
   */
  protected $innerService;

  /**
   * Intercepts all method calls not defined in the decorator.
   *
   * @param string $method
   *   Name of the method.
   * @param array|mixed $args
   *   Arguments directed to the method.
   *
   * @return mixed
   *   The results of the method call.
   */
  public function __call($method, $args) {
    return call_user_func_array([$this->innerService, $method], $args);
  }

  /**
   * Constructs the LanguageConfigFactoryDecorator object.
   *
   * @param \Drupal\language\Config\LanguageConfigFactoryOverrideInterface $inner_service
   *   The original service.
   * @param \Drupal\Core\Config\StorageInterface $storage
   *   The configuration storage engine.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   An event dispatcher instance to use for configuration events.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config
   *   The typed configuration manager.
   * @param \Drupal\Core\Language\LanguageDefault $default_language
   *   The default language.
   */
  public function __construct(LanguageConfigFactoryOverrideInterface $inner_service, StorageInterface $storage, EventDispatcherInterface $event_dispatcher, TypedConfigManagerInterface $typed_config, LanguageDefault $default_language) {
    $this->innerService = $inner_service;
    parent::__construct($storage, $event_dispatcher, $typed_config, $default_language);
  }

  /**
   * Prevents Localized Config overrides from being deleted with the original.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The config CRUD event.
   */
  public function onConfigSave(ConfigCrudEvent $event) {
    $config = $event->getConfig();
    $name = $config->getName();
    if (strpos($name, 'localized_config') !== 0) {
      foreach (\Drupal::languageManager()->getLanguages() as $language) {
        $config_translation = $this->getOverride($language->getId(), $name);
        if (!$config_translation->isNew()) {
          $this->filterOverride($config, $config_translation);
        }
      }
    }
  }

}
