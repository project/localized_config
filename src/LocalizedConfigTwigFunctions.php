<?php

namespace Drupal\localized_config;

use Twig\Extension\ExtensionInterface;

/**
 * Extends twig functions.
 */
class LocalizedConfigTwigFunctions extends \Twig_Extension implements ExtensionInterface {

  /**
   * Localized config helper service (localized_config.helper).
   *
   * @var \Drupal\localized_config\LocalizedConfigHelper
   */
  protected $configHelper;

  /**
   * LocalizedConfigTwigFunctions constructor.
   *
   * @param \Drupal\localized_config\LocalizedConfigHelper $configHelper
   *   Expects the localized_config.helper service.
   */
  public function __construct(LocalizedConfigHelper $configHelper) {
    $this->configHelper = $configHelper;
  }

  /**
   * Returns a converted string from camel to snake case.
   *
   * @param string $input
   *   Expects a string to convert to snake case.
   *
   * @return string
   *   Returns the converted string.
   */
  public function camelCaseToSnakeCase(string $input): string {
    preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
    $ret = $matches[0];
    foreach ($ret as &$match) {
      $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
    }
    return implode('_', $ret);
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return 'localized_config.twig_functions';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array {
    $methods = get_class_methods($this->configHelper);
    $functions = [];

    // Go through all functions inside the helper and create a corresponding
    // entry.
    foreach ($methods as $method) {
      if ($method === '__construct') {
        continue;
      }

      // Convert name into snake case.
      $snake_case = "localized_config_{$this->camelCaseToSnakeCase($method)}";
      $functions[] = new \Twig_SimpleFunction($snake_case, [
        $this->configHelper,
        $method,
      ]);
    }

    // Preserve old functionality.
    $functions[] = new \Twig_SimpleFunction('localized_config', [
      $this,
      'localizedConfig',
    ],
    );

    return $functions;
  }

  /**
   * Get a localized variable from the configs.
   *
   * @param string $plugin_name
   *   The Localized Config plugin which the config belongs to.
   * @param string|null $var_name
   *   The id of the value to fetch.
   * @param string|object|null|false $language
   *   The language to get the config from.
   *
   * @return mixed|null
   *   Return either the value, or NULL if none found.
   *
   * @deprecated in localized_config:1.0.0-alpha10 and is removed from
   *   localized_config:1.1.0 Use localized_config_get_variable instead.
   *
   * @see https://www.drupal.org/project/localized_config/issues/3188626
   * #comment-14032664
   */
  public function localizedConfig(string $plugin_name, string $var_name = NULL, string $language = NULL) {
    return $this->configHelper->getVariable($plugin_name, $var_name, $language);
  }

}
