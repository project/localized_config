<?php

namespace Drupal\localized_config;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Helper for locale settings/variable-related methods.
 *
 * @package Drupal\localized_config
 */
class LocalizedConfigHelper {

  /**
   * Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Language Manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Localized Config Plugin Manager service.
   *
   * @var \Drupal\localized_config\LocalizedConfigPluginManager
   */
  protected $pluginManager;

  /**
   * Are languages supported for this system?
   *
   * @var bool
   */
  protected $languagesSupported;

  /**
   * Cache Tags invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidator
   */
  protected $cacheTagsInvalidator;

  /**
   * Localized Config language-related methods.
   *
   * @var \Drupal\localized_config\LocalizedConfigLanguageHelper
   */
  protected $languageHelper;

  /**
   * LocalizedConfigHelper constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Factory service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language Manager service.
   * @param \Drupal\localized_config\LocalizedConfigPluginManager $localized_config_plugin_manager
   *   Localized Config Plugin Manager service.
   * @param \Drupal\Core\Cache\CacheTagsInvalidator $cache_tags_invalidator
   *   Cache Tags invalidator service.
   * @param \Drupal\localized_config\LocalizedConfigLanguageHelper $language_helper
   *   Localized Config language-related methods.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager, LocalizedConfigPluginManager $localized_config_plugin_manager, CacheTagsInvalidator $cache_tags_invalidator, LocalizedConfigLanguageHelper $language_helper) {
    $this->config = $config_factory;
    $this->languageManager = $language_manager;
    $this->pluginManager = $localized_config_plugin_manager;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->languageHelper = $language_helper;

    $this->languagesSupported = (bool) $this->config->get('localized_config.settings')
      ->get('enable_languages');
  }

  /**
   * Returns whether or not languages are supported on this system.
   *
   * @return bool
   *   The result.
   */
  public function languagesSupported() {
    return $this->languagesSupported;
  }

  /**
   * Get localized variables from all configs.
   *
   * @param string|object|null|false $language
   *   The language to get the config from.
   *
   * @return mixed|null
   *   Return either the value, or NULL if none found.
   */
  public function getAllVariables($language = NULL) {
    $variables = [];
    $plugin_definitions = $this->pluginManager->getDefinitions();
    foreach ($plugin_definitions as $plugin => $def) {
      $variables[$plugin] = $this->getVariable($plugin, NULL, $language);
    }
    return $variables;
  }

  /**
   * Get a localized variable from the configs.
   *
   * @param string $plugin_name
   *   The Localized Config plugin which the config belongs to.
   * @param string|null $var_name
   *   The id of the value to fetch.
   * @param string|object|null|false $language
   *   The language to get the config from.
   *
   * @return mixed|null
   *   Return either the value, or NULL if none found.
   */
  public function getVariable($plugin_name, $var_name = NULL, $language = NULL) {
    $language_manager = $this->languageManager;
    $original_language = $language_manager->getConfigOverrideLanguage();
    $variable = FALSE;
    // Fetch the language.
    if ($language !== FALSE) {
      $language = $this->getApplicableLanguageObject($language);
      if ($language) {
        $language_manager->setConfigOverrideLanguage($language);
      }
    }

    $config = $this->config->get('localized_config.' . $plugin_name);

    if ($config && $language !== FALSE) {
      // Fetch the variable in the specified language.
      $variable = $config->get($var_name);
    }

    if (!isset($variable) || !$variable) {
      // Fetch the original ('global') variable.
      $variable = $config->getOriginal($var_name, FALSE);
    }

    $language_manager->setConfigOverrideLanguage($original_language);
    return $variable;
  }

  /**
   * Fetch the global value of a certain variable.
   *
   * @param string $plugin_name
   *   Name of the plugin to fetch from.
   * @param string|null $var_name
   *   Name of the variable to fetch.
   *
   * @return mixed|null
   *   The global variable.
   */
  public function getGlobalVariable($plugin_name, $var_name) {
    return $this->getVariable($plugin_name, $var_name, FALSE);
  }

  /**
   * Fetch an array of variables from a set of languages.
   *
   * @param string $plugin_name
   *   Name of the plugin to fetch from.
   * @param string|null $var_name
   *   Name of the variable to fetch.
   * @param array[LanguageInterface] $languages
   *   An array of languages to fetch the variables from, keyed by langcode.
   *
   * @return array
   *   An array of variables, keyed by langcode.
   */
  public function getVariableFromLanguages($plugin_name, $var_name = NULL, array $languages = []) {
    if (empty($languages)) {
      $language_manager = $this->languageManager;
      $languages = $language_manager->getLanguages();
    }
    $result = [];
    foreach ($languages as $langcode => $language) {
      $result[$langcode] = $this->getVariable($plugin_name, $var_name, $language);
    }

    return $result;
  }

  /**
   * Checks whether a given Localized Config plugin is enabled.
   *
   * @param string $plugin_name
   *   Which config to fetch a value from?
   * @param string|object|null $language
   *   The language to get the config from.
   *
   * @return bool
   *   Returns true or false.
   */
  public function pluginEnabled($plugin_name, $language = NULL) {
    $var = $this->getVariable($plugin_name, 'enabled', $language);
    return $var ? TRUE : FALSE;
  }

  /**
   * Provides a list of all languages for which a certain plugin is enabled.
   *
   * @param string $plugin_name
   *   Which config to fetch the enabled state for?
   *
   * @return \Drupal\Core\Language\LanguageInterface[]
   *   An array of language objects, keyed by langcode.
   */
  public function getEnabledLanguages($plugin_name) {
    $language_manager = $this->languageManager;
    $languages = $language_manager->getLanguages();
    $result = [];
    foreach ($languages as $langcode => $language) {
      if ($this->pluginEnabled($plugin_name, $language)) {
        $result[$langcode] = $language;
      }
    }

    return $result;
  }

  /**
   * Receives langcode or language object and returns an object.
   *
   * @param string|\Drupal\Core\Language\LanguageInterface|null $language
   *   The language to be tested.
   *
   * @return \Drupal\Core\Language\LanguageInterface|false
   *   The language, or FALSE if no such language present.
   */
  protected function getApplicableLanguageObject($language) {
    $language_manager = $this->languageManager;
    if ($language instanceof LanguageInterface) {
      return $language;
    }

    $cache = &drupal_static(__FUNCTION__ . ':' . ($language ?? 'null'));
    if ($cache) {
      return $cache;
    }

    if ($language) {
      if (\is_string($language)) {
        $language_object = $language_manager->getLanguage($language);
        if ($language_object) {

          $cache = $language_object;
          return $language_object;
        }
      }
    }

    $lang = $language_manager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT);
    $cache = $lang;

    return $lang;
  }

  /**
   * Adds cache information about a Localized Config to something.
   *
   * @param string $plugin_name
   *   Which config to apply cache information from?
   * @param array|\Drupal\Core\Cache\CacheableMetadata $target
   *   The target config or render array.
   */
  public function addConfigCache($plugin_name, &$target) {
    if ($target instanceof CacheableMetadata) {
      $metadata = $target;
    }
    else {
      $metadata = CacheableMetadata::createFromRenderArray($target);
    }

    $metadata->addCacheTags([
      "localized_config.{$plugin_name}",
    ]);

    if (\is_array($target)) {
      $metadata->applyTo($target);
    }
  }

  /**
   * Invalidates the cache of a certain Localized Config.
   *
   * @param string $plugin_name
   *   Which config to invalidate the cache for?
   */
  public function invalidateConfigCache($plugin_name) {
    $this->cacheTagsInvalidator->invalidateTags(["localized_config.{$plugin_name}"]);
  }

}
