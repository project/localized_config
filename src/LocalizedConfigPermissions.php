<?php

namespace Drupal\localized_config;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for each localized config plugin.
 */
class LocalizedConfigPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The plugin manager service.
   *
   * @var \Drupal\localized_config\LocalizedConfigPluginManager
   */
  protected $pluginManager;

  /**
   * The Localized Configuration helper service.
   *
   * @var \Drupal\localized_config\LocalizedConfigHelper
   */
  protected $localizedConfigHelper;

  /**
   * LocalizedConfigPermissions constructor.
   *
   * @param \Drupal\localized_config\LocalizedConfigPluginManager $plugin_manager
   *   The plugin manager service.
   * @param \Drupal\localized_config\LocalizedConfigHelper $localized_config_helper
   *   The Localized Configuration helper service.
   */
  public function __construct(LocalizedConfigPluginManager $plugin_manager, LocalizedConfigHelper $localized_config_helper) {
    $this->pluginManager = $plugin_manager;
    $this->localizedConfigHelper = $localized_config_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.localized_config'),
      $container->get('localized_config.helper')
    );
  }

  /**
   * Defines dynamic permissions for the Localized Configuration plugins.
   *
   * @return array
   *   An array of dynamically defined permissions.
   */
  public function pluginPermissions() {
    $permissions = [];

    // Get all defined plugins and create permission.
    $plugin_definitions = $this->pluginManager->getDefinitions();
    if ($plugin_definitions) {
      foreach ($plugin_definitions as $plugin_id => $values) {
        $permissions['access localized config ' . $plugin_id] = $this->t('Access Localized Configuration plugin "@plugin_label"', ['@plugin_label' => $values['title']]);
      }
    }

    // If languages supported, add "Global" permisssion.
    if ($this->localizedConfigHelper->languagesSupported()) {
      $permissions['access global localized config'] = [
        'title' => $this->t('Access global values in Localized Configuration'),
        'description' => $this->t('Allows access to global, non-locale-specific configs in the Localized Configuration interface.'),
      ];
    }

    return $permissions;
  }

}
