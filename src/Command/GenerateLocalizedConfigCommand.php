<?php

namespace Drupal\localized_config\Command;

use Drupal\Console\Core\Utils\StringConverter;
use Drupal\Console\Extension\Manager;
use Drupal\Console\Utils\Validator;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Render\ElementInfoManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\Command;
use Drupal\Console\Core\Generator\GeneratorInterface;
use Drupal\Console\Command\Shared\ArrayInputTrait;
use Drupal\Console\Command\Shared\FormTrait;
use Drupal\Console\Command\Shared\ModuleTrait;

/**
 * Class GenerateLocalizedConfigCommand.
 *
 * Drupal\Console\Annotations\DrupalCommand (
 *     extension="localized_config",
 *     extensionType="module"
 * )
 */
class GenerateLocalizedConfigCommand extends Command {

  use ModuleTrait;
  use ArrayInputTrait;
  use FormTrait;

  /**
   * Drupal\Console\Core\Generator\GeneratorInterface definition.
   *
   * @var \Drupal\Console\Core\Generator\GeneratorInterface
   */
  protected $generator;

  /**
   * List of module information.
   *
   * @var array
   */
  private $moduleInfo;

  /**
   * Drupal console class, needed for some traits.
   *
   * @var \Drupal\Console\Extension\Manager
   */
  protected $extensionManager;

  /**
   * Drupal console class, needed for some traits.
   *
   * @var \Drupal\Console\Core\Utils\StringConverter
   */
  protected $stringConverter;

  /**
   * Drupal console validator, used to validate some command line parameters.
   *
   * @var \Drupal\Console\Utils\Validator
   */
  protected $validator;

  /**
   * Drupal class, needed for some traits.
   *
   * @var \Drupal\Core\Render\ElementInfoManager
   */
  protected $elementInfoManager;

  /**
   * Constructs a new GenerateLocalizedConfigCommand object.
   */
  public function __construct(GeneratorInterface $localizedConfigGenerator, Manager $extensionManager, StringConverter $stringConverter, Validator $validator, ElementInfoManager $elementInfoManager, ExtensionList $extensionList) {
    $this->generator = $localizedConfigGenerator;
    $this->moduleInfo = $extensionList->getAllInstalledInfo();
    $this->extensionManager = $extensionManager;
    $this->stringConverter = $stringConverter;
    $this->validator = $validator;
    $this->elementInfoManager = $elementInfoManager;
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this->setName('generate:plugin:localizedconfig');
    $this->setDescription('Localized config plugin generator');
    $this->setAliases(['glc']);

    $this->addOption('module', NULL, InputOption::VALUE_REQUIRED, 'Module machine name');
    $this->addOption('class', NULL, InputOption::VALUE_REQUIRED, 'Plugin class name');
    $this->addOption('title', NULL, InputOption::VALUE_OPTIONAL, 'Plugin title');
    $this->addOption('id', NULL, InputOption::VALUE_OPTIONAL, 'Plugin ID');
    $this->addOption('weight', NULL, InputOption::VALUE_OPTIONAL, 'Plugin weight');
    $this->addOption('disabled', NULL, InputOption::VALUE_OPTIONAL, 'Plugin is disabled');
    $this->addOption('inputs', NULL, InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Configuration inputs');
  }

  /**
   * {@inheritdoc}
   */
  protected function interact(InputInterface $input, OutputInterface $output) {
    $module = $input->getOption('module');
    $class = $input->getOption('class');
    $title = $input->getOption('title');
    $id = $input->getOption('id');
    $weight = $input->getOption('weight');
    $disabled = $input->getOption('disabled');

    $inputs = $input->getOption('inputs');
    if ($inputs !== NULL) {
      $inputs = $this->explodeInlineArray($inputs);
    }

    // Validate/reset the command line parameters.
    if ($module !== NULL) {
      try {
        if (!$this->isValid('module', $module)) {
          $module = NULL;
        }
      }
      catch (\InvalidArgumentException $e) {
        $module = NULL;
      }
    }
    if ($id !== NULL) {
      try {
        if (!$this->isValid('id', $id)) {
          $id = $module;
        }
      }
      catch (\InvalidArgumentException $e) {
        $id = $module;
      }
    }
    if ($weight !== NULL) {
      try {
        if (!$this->isValid('numeric', $weight)) {
          $weight = 0;
        }
      }
      catch (\InvalidArgumentException $e) {
        $weight = 0;
      }
    }
    if ($disabled !== NULL) {
      try {
        if (!$this->isValid('flag', $disabled)) {
          $disabled = 0;
        }
      }
      catch (\InvalidArgumentException $e) {
        $disabled = 0;
      }
    }

    // Get and validate the module machine name if not supplied via parameter.
    if ($module === NULL) {
      $module = $this->getModuleOption();
    }

    // Get information about the module.
    $module_info = $this->getModuleInfo($module);

    // Get the plugin class name if not supplied via parameter.
    if ($class === NULL) {
      // Use the camel cased module machine name as class name for default.
      $class_default = str_replace(['_', '-'], ' ', $module);
      $class_default = ucwords($class_default);
      $class_default = str_replace(' ', '', $class_default);
      $class_default .= 'LocalizedConfig';

      $class = $this->getIo()
        ->ask('Plugin class name', $class_default);
    }
    // Meta: get the plugin title.
    if ($title === NULL) {
      $title = $this->getIo()->ask('Plugin title', $module_info['name']);
    }
    // Meta: get and validate the plugin ID.
    if ($id === NULL) {
      $id = $this->getIo()
        ->ask('Plugin ID', $module, function ($id) {
          return $this->isValid('id', $id);
        });
    }
    // Meta: get and validate the plugin weight.
    if ($weight === NULL) {
      $weight = $this->getIo()
        ->ask('Plugin weight', 0, function ($weight) {
          return $this->isValid('numeric', $weight);
        });
    }
    // Meta: get and validate the plugin status (disabled).
    if ($disabled === NULL) {
      $disabled = $this->getIo()
        ->ask('Plugin disabled?', 0, function ($disabled) {
          return $this->isValid('flag', $disabled);
        });
    }
    // Input fields.
    if (empty($inputs)) {
      $inputs = $this->formQuestion();
    }

    $input->setOption('module', $module);
    $input->setOption('class', $class);
    $input->setOption('title', $title);
    $input->setOption('id', $id);
    $input->setOption('weight', $weight);
    $input->setOption('disabled', $disabled);
    $input->setOption('inputs', $inputs);
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    // Get the parameters for the generator.
    $parameters = $input->getOptions();

    // Get information about the module the plugin is to be placed in.
    $module_info = $this->getModuleInfo($parameters['module']);
    $parameters['name'] = $module_info['name'];
    $parameters['module_path'] = $module_info['module_path'];

    // Generate the files from templates.
    $this->generator->generate($parameters);

    $this->getIo()->info('Localized config plugin generated.');
  }

  /**
   * Check if a parameter is valid.
   *
   * @param string $type
   *   Parameter type. Currently supported is 'module'.
   * @param string $value
   *   Value to be validated.
   *
   * @return string
   *   Validated parameter value.
   *
   * @throws \InvalidArgumentException
   *   If the parameter is not valid.
   */
  public function isValid($type, $value) {
    // Supported types to be validated.
    // Supported validator types are currently:
    // * regex - Use a regulare expression to validate.
    // * validator - Use a method of the class Drupal\Console\Utils\Validator.
    // * moduleInfoExists - Check if information about this module exists.
    // * numeric - Checks if numeric.
    $validation = [
      'module' => [
        'name' => 'Module machine name',
        'validators' => [
          ['type' => 'validator', 'value' => 'validateMachineName'],
          ['type' => 'moduleInfoExists'],
        ],
      ],
      'id' => [
        'name' => 'Plugin ID',
        'validators' => [
          ['type' => 'validator', 'value' => 'validateMachineName'],
        ],
      ],
      'numeric' => [
        'name' => 'Number (integer)',
        'validators' => [
          ['type' => 'numeric'],
        ],
      ],
      'flag' => [
        'name' => 'Flag (0/1)',
        'validators' => [
          ['type' => 'regex', 'value' => '/^[01]$/'],
        ],
      ],
    ];

    // Check if we have a valid validation type.
    if (!isset($validation[$type])) {
      throw new \InvalidArgumentException(
        sprintf('Invalid input type "%s".', $type)
      );
    }

    // Per default the value is invalid.
    $isValid = FALSE;
    // If we have validators for this type the value is valid until
    // at least one validation failed.
    if (count($validation[$type]['validators']) > 0) {
      $isValid = TRUE;
    }

    foreach ($validation[$type]['validators'] as $validator) {
      switch ($validator['type']) {
        // Regular expression validator.
        case 'regex':
          if (!preg_match($validator['value'], $value)) {
            $isValid = FALSE;
          }
          break;

        // Validate using the Drupal\Console\Utils\Validator class.
        case 'validator':
          if (!$this->validator->{$validator['value']}($value)) {
            $isValid = FALSE;
          };
          break;

        // Validate by checking if module info for this name exists.
        case 'moduleInfoExists':
          if (!$this->moduleInfoExists($value)) {
            $isValid = FALSE;
          }
          break;

        // Check if numeric.
        case 'numeric':
          if (!is_numeric($value)) {
            $isValid = FALSE;
          }
          break;
      }
    }

    // If the value is valid return unchanged.
    if ($isValid) {
      return $value;
    }

    // If invalid throw an exception.
    throw new \InvalidArgumentException(
      sprintf('Invalid value "%s" for %s.', $value, $validation[$type]['name'])
    );
  }

  /**
   * Check if information about this module exists.
   *
   * @param string $module
   *   Module machine name.
   *
   * @return bool
   *   Boolean if module info exists.
   */
  private function moduleInfoExists($module) {
    $module_info = $this->getModuleInfo($module);
    return $module_info !== NULL;
  }

  /**
   * Get information about this module including relative path.
   *
   * @param string $module
   *   Module machine name to be checked.
   *
   * @return array|null
   *   Array if there is information about the module, NULL if not.
   */
  private function getModuleInfo($module) {
    // If the module is not listed, return NULL.
    if (!isset($this->moduleInfo[$module])) {
      return NULL;
    }

    $module_info = $this->moduleInfo[$module];
    // Add information about the relative module path.
    $module_info['module_path'] = drupal_get_path('module', $module);

    return $module_info;
  }

}
