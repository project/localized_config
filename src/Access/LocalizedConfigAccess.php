<?php

namespace Drupal\localized_config\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\localized_config\LocalizedConfigHelper;
use Drupal\localized_config\LocalizedConfigLanguageHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Checks access to certain pages of the Localized Config forms.
 *
 * @package Drupal\localized_config\Access
 */
class LocalizedConfigAccess implements AccessInterface, ContainerInjectionInterface {
  /**
   * The localized config helper service.
   *
   * @var \Drupal\localized_config\LocalizedConfigHelper
   */
  protected $localizedConfigHelper;

  /**
   * Localized Config language-related methods.
   *
   * @var \Drupal\localized_config\LocalizedConfigLanguageHelper
   */
  protected $languageHelper;

  /**
   * Constructs a new bubbling URL generator service.
   *
   * @param \Drupal\localized_config\LocalizedConfigHelper $localized_config_helper
   *   The localized config helper service.
   * @param \Drupal\localized_config\LocalizedConfigLanguageHelper $language_helper
   *   Localized Config language-related methods.
   */
  public function __construct(LocalizedConfigHelper $localized_config_helper, LocalizedConfigLanguageHelper $language_helper) {
    $this->localizedConfigHelper = $localized_config_helper;
    $this->languageHelper = $language_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('localized_config.helper'),
      $container->get('localized_config.language_helper')
    );
  }

  /**
   * The main access callback.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account.
   * @param string|null $language
   *   The language that's being accessed.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultForbidden
   *   The access result.
   */
  public function access(AccountInterface $account, $language = NULL) {
    if ($language) {
      $languages = $this->languageHelper->getLanguagesOfUser($account);
      $languages = $this->languageHelper->languagesToReadableArray($languages);

      if (!array_key_exists($language, $languages)) {
        return AccessResult::forbidden();
      }
    }
    else {
      $permission_check = ['access localized config'];
      if ($this->localizedConfigHelper->languagesSupported()) {
        $permission_check[] = 'access global localized config';
      }
      return AccessResult::allowedIfHasPermissions($account, $permission_check);
    }

    return AccessResult::allowedIfHasPermission($account, 'access localized config');
  }

}
