<?php

/**
 * @file
 * Hooks related to the Localized Configuration system.
 */

use Drupal\user\UserInterface;

/**
 * Allows changing the list of languages available to Localized Config.
 *
 * @param \Drupal\Core\Language\LanguageInterface[] $languages
 *   Array of languages, keyed by langcode.
 */
function hook_localized_config_language_list_alter(array &$languages) {

  // Always remove English.
  unset($languages['en']);
}

/**
 * Allows changing which languages a user identifies with.
 *
 * @param \Drupal\Core\Language\LanguageInterface[] $languages
 *   Array of languages, keyed by langcode.
 * @param \Drupal\user\UserInterface $user
 *   The user object.
 */
function hook_localized_config_user_languages_alter(array &$languages, UserInterface $user) {

  // If there is a user language field, limit the languages array by only the
  // enabled languages for current user.
  if ($user->hasField('%user_language_field')) {
    $user_languages = [];
    foreach ($user->get('%user_language_field') as $item) {
      $language = $item->entity;
      $user_languages[$language->getId()] = $language;
    }
    $languages = array_intersect_key($languages, $user_languages);
  }
}
