INTRODUCTION
------------

The Localized Configuration module is a custom layer on top of Drupal's
configuration management system, allowing to build a centralized configuration
interface for your website with reduced effort. Localized Configuration does a
lot of the footwork in regards to storing configuration and, most importantly,
handles differences between different locales and languages in the Drupal
environment, making it especially valuable if you use languages in
a multisite context.

This module is intended as a library for developers and does not
provide meaningful functionality on its own for site-builders.


REQUIREMENTS
------------

This module requires the core Language module to be enabled.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/documentation/install/modules-themes/modules-8
for further information.


CONFIGURATION
-------------

* Configure the module in Configuration » Localized configuration » Settings:

    "Enabled language support": Toggle this checkbox to enable language tabs
    in Localized Configuration. Toggling this off will limit the interface to
    global configuration.

    "Filter by site code": Remove languages from the interface whose language
    codes are not written in format "language-sitename", e.g. "de-kk".  This is
    useful if you use languages for multisite purposes.

* Configure permissions in People » Permissions:

    * "Access Localized Configuration": Allows viewing the
    Localized Configuration interface.
    * "Enable/disable Localized Configuration plugins": Allows
    enabling/disabling the state of Localized Configuration plugins.
    * "Configure Localized Configuration settings": Allows viewing the
    Localized Configuration settings form.

   Additionally, each enabled Localized Configuration plugin will receive its
   own permission which has to be toggled for the respective roles.


USAGE
-----

The module is intended for developers as a base for their
own configuration plugins.

In your custom modules you wish to use Localized Configuration with,
implement a Localized Config plugin extending the class
`LocalizedConfigPluginInterface` under `src/Plugins/LocalizedConfig`. 

#### DrupalConsole integration

This module integrates with DrupalConsole to provide a simple generator for
new plugins. Simply execute `drupal generate:plugin:localized_config` in the
command line to run the generator wizard.

#### Example plugin

This module comes bundled with a sub-module which implements a simple
example plugin. Enable the module `localized_config_example` to view it.

#### Metadata

Each plugin contains a set of annotation-based metadata:

 * `title`: The human-readable title of the configuration.
 * `id`: The machine-readable title of the configuration.
 * `global_only`: A boolean (0/1) which determines whether this
   configuration is available only on the global level, or across
   locales. This allows the Localized Configuration to be used as a
   general repository of customer configurations even without making
   use of the locale-based aspect.
 * `enabled`: A boolean (0/1) which simply turns the plugin on or off.
 
#### Methods

The plugin interface handles the configuration form itself and
contains multiple methods which must be implemented:

 * The `add` method in the plugin is responsible for generating
 the form.
 * The `validate` method validates any input to the form.
 * The `submit` method saves the input to the configuration.
 * Specifics of handling languages etc. are taken care of by the module.
 
#### Storage
 
The module handles the storage of the configuration. Configuration is
stored across multiple possible files. In the following examples,
`PLUGINNAME` is the stand-in for the name of the plugin in question.
 
 * `localized_config.PLUGINNAME.yml`: The global settings configured in
   the plugin.
 * `languages/LANGCODE/localized_config.PLUGINNAME.yml`:
   Language-specific settings for the plugin.
   
#### Reading and using the configuration

Once the plugin is configured and configuration is saved, it needs to be
implemented for usage. This happens with the `LocalizedConfigHelper`
service class that comes with the module. It can be called as follows:

```php
/** @var
* \Drupal\localized_config\LocalizedConfigHelper $localized_config_helper 
*/
$config = \Drupal::service('localized_config.helper');
```

The service contains methods for reading and manipulating
the localized configuration, most prominently among them:

* `getVariable($module_name, $var_name, $language)`

  The most common method, which loads a variable from
  certain configuration.
  
  * `$module_name` is the ID of the plugin to be loaded.
  * (Optional) `$var_name` is the variable name to fetch from the
    configuration. If omitted, an array of all configuration of
    the plugin is returned.
  * (Optional) `$language` lets you decide the language from which to
    load the configuration. If omitted, a priority system will be used
    to fetch the configuration from the *current* context.  
    
    If FALSE, will fetch exclusively the "global" configuration.
    
* `getGlobalVariable($module_name, $var_name)`
  
  A wrapper for `getVariable` which always sets `$language` to FALSE,
  forcing the "global" variable to be loaded.
  
* `moduleEnabled($module_name, $language)`

  Returns TRUE or FALSE for whether or not a configuration plugin is
  enabled at all. Optional parameter for language
  allows to check enabled status for certain locales.
  
* `addConfigCache($module_name, &$target)`

  This helper method sets cache tags specific to the selected
  configuration onto the `$target`, which should be either
  a render array or a `CacheableMetadata` object.
  
  This allows certain elements to immediately update on a cached system
  when changes are made to the configuration.
